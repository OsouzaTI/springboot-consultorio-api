package com.osouza.consultorio.paciente;

import java.util.Optional;

import com.osouza.consultorio.cadastro.Endereco;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Table(name = "pacientes")
@Entity(name = "Paciente")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Paciente {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String email;
    private String telefone;
    private String cpf;
    private Boolean ativo;

    @Embedded
    private Endereco endereco;

    public Paciente(DadosCadastroPaciente dados) {
        this.nome = dados.nome();
        this.email = dados.email();
        this.telefone = dados.telefone();
        this.cpf = dados.cpf();
        this.endereco = new Endereco(dados.endereco());
    }

    public void atualizar(DadosAtualizacaoPaciente dados) {
        this.nome = Optional.ofNullable(dados.nome()).orElse(this.nome);
        this.email = Optional.ofNullable(dados.email()).orElse(this.email);
        if(dados.endereco() != null) {
            this.endereco = Optional.ofNullable(new Endereco(dados.endereco())).orElse(this.endereco);
        }
    }

    public void ativar() { this.ativo = true; }
    public void desativar() { this.ativo = false; }

}
