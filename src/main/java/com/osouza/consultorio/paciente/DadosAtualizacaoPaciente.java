package com.osouza.consultorio.paciente;

import com.osouza.consultorio.cadastro.DadosEndereco;

import jakarta.validation.Valid;

public record DadosAtualizacaoPaciente(
    Long id,
    String nome,
    String email,
    String cpf,

    @Valid
    DadosEndereco endereco

){}
