package com.osouza.consultorio.medico;

import java.util.Optional;

import com.osouza.consultorio.cadastro.Endereco;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Table(name = "medicos")
@Entity(name = "Medico")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Medico {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String email;
    private String telefone;
    private String crm;
    private Boolean ativo;

    @Enumerated(EnumType.STRING)
    private Especialidade especialidade;

    @Embedded
    private Endereco endereco;
    
    public Medico(DadosCadastroMedico dados) {
        this.nome = dados.nome();
        this.email = dados.email();
        this.telefone = dados.telefone();
        this.crm = dados.crm();        
        this.especialidade = dados.especialidade();
        this.endereco = new Endereco(dados.endereco());
    }

    public void atualizar(DadosAtualizacaoMedico dados) {
        this.nome = Optional.ofNullable(dados.nome()).orElse(this.nome);
        this.email = Optional.ofNullable(dados.email()).orElse(this.email);
        if(dados.endereco() != null) {
            this.endereco = Optional.ofNullable(new Endereco(dados.endereco())).orElse(this.endereco);
        }
    }

    public void ativar() { this.ativo = true; }
    public void desativar() { this.ativo = false; }
}