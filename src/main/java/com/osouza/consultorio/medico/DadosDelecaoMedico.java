package com.osouza.consultorio.medico;

import jakarta.validation.constraints.NotNull;

public record DadosDelecaoMedico(
    @NotNull
    Long id
) {}
