package com.osouza.consultorio.medico;

import com.osouza.consultorio.cadastro.DadosEndereco;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

public record DadosAtualizacaoMedico(
    @NotNull
    Long id,
    String nome,
    String email,
    String telefone,
    @Valid
    DadosEndereco endereco
) {}
