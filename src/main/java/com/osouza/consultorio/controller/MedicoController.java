package com.osouza.consultorio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.osouza.consultorio.medico.DadosAtualizacaoMedico;
import com.osouza.consultorio.medico.DadosCadastroMedico;
import com.osouza.consultorio.medico.DadosDelecaoMedico;
import com.osouza.consultorio.medico.DadosListagemMedico;
import com.osouza.consultorio.medico.Medico;
import com.osouza.consultorio.medico.MedicoRepository;

import jakarta.validation.Valid;


@RestController
@RequestMapping("/medicos")
public class MedicoController {
    
    @Autowired
    MedicoRepository repository;

    @PostMapping("/cadastrar")
    @Transactional
    public void cadastrar(@RequestBody @Valid DadosCadastroMedico dados) {
        // salvando os dados do médico
        repository.save(new Medico(dados));
    }

    @GetMapping("/listar")
    @Transactional
    public Page<DadosListagemMedico> listar(@PageableDefault(size = 10, sort = {"nome"}) Pageable paginacao) {
        // versão sem paginação
        //return repository.findAll().stream().map(DadosListagemMedico::new).toList();
        return repository.findAllByAtivoTrue(paginacao).map(DadosListagemMedico::new);
    }

    @PutMapping("/atualizar")
    @Transactional
    public DadosListagemMedico atualizar(@RequestBody @Valid DadosAtualizacaoMedico dados) {
        var medico = repository.getReferenceById(dados.id());
        medico.atualizar(dados);
        return new DadosListagemMedico(medico);
    }

    @PatchMapping("/ativar")
    @Transactional
    public void ativar(@RequestBody @Valid DadosDelecaoMedico dados) {
        var medico = repository.getReferenceById(dados.id());
        // ativação
        medico.ativar();
    }

    @DeleteMapping("/deletar")
    @Transactional
    public void deletar(@RequestBody @Valid DadosDelecaoMedico dados) {
        var medico = repository.getReferenceById(dados.id());
        // deleção lógica
        medico.desativar();
    }

}
